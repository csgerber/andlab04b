package edu.uchicago.gerber.andlab04b;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import edu.uchicago.gerber.andlab04b.model.Reminder;
import edu.uchicago.gerber.andlab04b.model.RemindersDbAdapter;
import edu.uchicago.gerber.andlab04b.model.RemindersSimpleCursorAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class BasicFragment extends Fragment {

    private ListView mListView;
    private FloatingActionButton mFab;


    private RemindersSimpleCursorAdapter mCursorAdapter;

    public BasicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            //Clear all data
            ((NavActivity) getActivity()).getDbAdapter().deleteAllReminders();
            //Add some data
            ((NavActivity) getActivity()).insertSomeReminders();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //influate the root view of the fragment
        View view = inflater.inflate(R.layout.fragment_basic, container, false);

        mFab = (FloatingActionButton) view.findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        //get a reference to the inflated listview using the id
        mListView = (ListView) view.findViewById(R.id.listView);


        Cursor cursor =   ((NavActivity) getActivity()).getDbAdapter().fetchAllReminders();
        //from columns defined in the db
        String[] from = new String[]{
                RemindersDbAdapter.COL_CONTENT};
        //to the ids of views in the layout
        int[] to = new int[]{R.id.row_text};

        mCursorAdapter = new RemindersSimpleCursorAdapter(
                //context
                getActivity(),
                //the layout of the row
                R.layout.reminders_row,
                //cursor
                cursor,
                //from columns defined in the db
                from,
                //to the ids of views in the layout
                to,
                //flag - not used
                0);
        //the cursorAdapter (controller) is now updating the listView (view)
        //with data from the db(model)
        mListView.setAdapter(mCursorAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int nId = getIdFromPosition(position);
                Reminder reminder =   ((NavActivity) getActivity()).getDbAdapter().fetchReminderById(nId);
                String strVaule = reminder.getContent();

                //la;uch new fragment with the value
                Log.d("UUXX", strVaule);
                // getActivity().getSupportFragmentManager()
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_pane, DetailBasicFragment.newInstance(strVaule))
                        .addToBackStack(DetailBasicFragment.class.getSimpleName())
                        .commit();

            }
        });


        return view;
    }



    private int getIdFromPosition(int nC) {
        return (int) mCursorAdapter.getItemId(nC);
    }

}
