package edu.uchicago.gerber.andlab04b;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailBasicFragment extends Fragment {

    public static final String KEY_TITLE = "KEY_TITLE";
    private String mTitle;
    private TextView mTextView;
    private Button mButtonBack;

    public static DetailBasicFragment newInstance(String strTitle){
        DetailBasicFragment detailBasicFragment = new DetailBasicFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TITLE, strTitle);
        detailBasicFragment.setArguments(bundle);
        return  detailBasicFragment;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null){
            mTitle = getArguments().getString(KEY_TITLE);
        }
    }

    public DetailBasicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_basic,
                container, false);

        mTextView = (TextView) view.findViewById(R.id.textView);
        mTextView.setText(mTitle);

        mButtonBack = (Button) view.findViewById(R.id.buttonBack);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }



}
